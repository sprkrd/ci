
import numpy as np
import os
import sys

from deap import algorithms
from deap import benchmarks
from deap import base
from deap import creator
from deap import tools
from deap import cma
import pickle

creator.create("FitnessMax", base.Fitness, weights=(1.0,))
creator.create("Individual", list, fitness=creator.FitnessMax, strategy=None)

def get_values(d='/home/salva/Documents/CI_FINAL'):
    folders=[o for o in os.listdir(d) if os.path.isdir(os.path.join(d,o))]
    allresults = [];
    for folder in folders:
        fullfolder=os.path.join(d, folder)
        splitted = folder.split("_")
        if len(splitted) < 4:
            break
        lambda_ = splitted[1]
        mu = splitted[2]
        sigma = splitted[3]
        with open("{}/checkpoint_{}_{}_{}.pkl".format(fullfolder,lambda_, mu, sigma), 'rb') as f:
            cp = pickle.load(f)
        elapsed = cp['elapsed']
        strategy = cp['strategy']
        gen = cp["generation"]
        closefunvalues = cp['closefunvalues']
        best_values = cp["best_values"]
        median_values = cp["median_values"]
        halloffame = cp["halloffame"]
        logbook = cp["logbook"]
        np.random.set_state(cp["rndstate"])

        best_fitness= halloffame[0].fitness.values[0]
        n_individuals = (gen)*(int(lambda_))
        n_individuals_2 = None
        
        if best_fitness > 2.0:
            for g in range(1,gen+1):
                with open("{}/population_gen_{:04d}.pkl".format(fullfolder, g), 'rb') as f:
                    cp = pickle.load(f)
                    population = cp["population"]
                    best_pop = population[0].fitness.values[0]
                    if best_pop > 2.0:
                        n_individuals_2=(g)*(int(lambda_))
                        break
        aux=(float(sigma), int(lambda_), int(mu), best_fitness, n_individuals, n_individuals_2)
        allresults.append(aux)
    allresults.sort()
    return allresults

def print_table(allresults):
    print "$\\sigma$ & $\\lambda$ & $\\mu$ & Best $f$ & \\# Individuals & \\# Individuals for $f>=2$ \\\\"
    for result in allresults:
        print "{:.02f} & {} & {} & {:.03f} & {} & {} \\\\".format(result[0], result[1], result[2], result[3], result[4], result[5])

# Deprecated
def print_2d(allresults):
    print "parameters"

    partial = []
    prev=allresults[0][1]
    for result in allresults:
        if result[1] != prev:
            print str(prev)+" & "+" & ".join(partial)
            partial=[]
            prev=result[1]
        partial.append(str(result[0])+':'+str(result[1])+':'+str(result[2]))
    if len(partial)>0:
        print str(allresults[-1][1])+" & "+" & ".join(partial)

    print "best_fitness"

    partial = []
    prev=allresults[0][1]
    for result in allresults:
        if result[1] != prev:
            print str(prev)+" & "+" & ".join(partial)
            partial=[]
            prev=result[1]
        partial.append("{:.03f}".format(result[3]))
    if len(partial)>0:
        print str(allresults[-1][1])+" & "+" & ".join(partial)

    print "n_individuals"

    partial = []
    prev=allresults[0][1]
    for result in allresults:
        if result[1] != prev:
            print str(prev)+" & "+" & ".join(partial)
            partial=[]
            prev=result[1]
        partial.append(str(result[4]))
    if len(partial)>0:
        print str(allresults[-1][1])+" & "+" & ".join(partial)

    print "n_individuals_2"

    partial = []
    prev=allresults[0][1]
    for result in allresults:
        if result[1] != prev:
            print str(prev)+" & "+" & ".join(partial)
            partial=[]
            prev=result[1]
        partial.append(str(result[5]))
    if len(partial)>0:
        print str(allresults[-1][1])+" & "+" & ".join(partial)

if __name__ == '__main__':
    try:
        folder = sys.argv[1]
    except Exception as e:
        print "Usage: ./get_table_values population_parent_folder"
        print "E.g. ./get_table_values checkpoints"
        sys.exit(-1)
    values = get_values(folder)
    print_table(values)
