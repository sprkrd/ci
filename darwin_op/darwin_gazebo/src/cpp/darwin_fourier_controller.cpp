#include <cmath>
#include <map>

#include <boost/bind.hpp>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <ros/ros.h>
#include <dynamic_reconfigure/server.h>
#include <darwin_gazebo/SimulateIndividual.h>
#include <darwin_gazebo/DarwinGazeboConfig.h>


namespace gazebo
{

class Oscillator
{
  private:
    double T_, A_, C_, phi_;

  public:
    static Oscillator DEFAULT;

    Oscillator(double T, double A, double C, double phi)
      : T_(T), A_(A), C_(C), phi_(phi)
    {}

    double operator()(double t) const
    {
      return C_ + A_*sin(2*M_PI*t/T_ + phi_);
    }

    Oscillator mirror() const
    {
      return Oscillator(T_, A_, -C_, phi_);
    }
};

Oscillator Oscillator::DEFAULT(1e9, 0, 0, 0);

typedef std::map<physics::JointPtr, Oscillator> OscillatorDict;
typedef OscillatorDict::iterator Iter;
typedef OscillatorDict::const_iterator CIter;

class DarwinExtra : public ModelPlugin
{
  private:
    OscillatorDict osc_dict_;

    physics::ModelPtr model_;
    physics::Link_V links_;
    event::ConnectionPtr updateConnection_;

    // ROS stuff
    ros::NodeHandle* nh_;
    ros::ServiceServer simulate_individual_srv_; // start simulation step by
                                                 // step server
    dynamic_reconfigure::Server<darwin_gazebo::DarwinGazeboConfig>* server_;
    darwin_gazebo::DarwinGazeboConfig config_;
    
    // Monitoring measures from last simulated individual
    std::vector<math::Vector3> rpy_;
    double distance_;
    int no_contact_counter_;

    double compute_theta()
    {
      math::Vector3 mean_rpy_(0,0,0);
      for (int i = 0; i < rpy_.size(); ++i)
      {
        mean_rpy_ += rpy_[i];
      }
      mean_rpy_ /= rpy_.size();

      double theta = 0;
      for (int i = 0; i < rpy_.size(); ++i)
      {
        theta += (rpy_[i] - mean_rpy_).GetSquaredLength();
      }
      theta = sqrt(theta/rpy_.size());

      return theta;
    }

    void computeModelCoG(math::Vector3& cog)
    {
      double mass = 0.0;
      cog = 0.0;
      for (int idx = 0; idx < links_.size(); ++idx)
      {
        double l_mass = links_[idx]->GetInertial()->GetMass();
        math::Vector3 l_cog = links_[idx]->GetWorldCoGPose().pos;
        cog += l_mass*l_cog;
        mass += l_mass;
      }
      cog /= mass;
    }

    void prepare_controller()
    {
      physics::Joint_V joints = model_->GetJoints();
      for (int i = 0; i < joints.size(); ++i)
      {
        joints[i]->SetMaxForce(0, 30.0);
        osc_dict_.insert(std::make_pair(joints[i], Oscillator::DEFAULT));
      }
    }

    bool set_oscillator(const std::string& joint_name,
        const Oscillator& oscillator)
    {
      physics::JointPtr joint = model_->GetJoint(joint_name);
      Iter it = osc_dict_.find(joint);
      if (not joint and it != osc_dict_.end())
      {
        return false;
      }
      it->second = oscillator;
      return true;
    }

    void set_joint_efforts(double t)
    {
      for (CIter it = osc_dict_.begin(); it != osc_dict_.end(); ++it)
      {
        // Simple proportional, hardcoded control :)
        double error = it->second(t) - it->first->GetAngle(0).Radian();
        it->first->SetForce(0, config_.P_gain*error);
      }
    }

    double getMinimumZ()
    {
      double z_min = 1e12;
      for (int idx = 0; idx < links_.size(); ++idx)
      {
        double z = links_[idx]->GetBoundingBox().min.z;
        if (z < z_min)
        {
          z_min = z;
        }
      }
      return z_min;
    }

    void nullify_twist()
    {
      for (int idx = 0; idx < links_.size(); ++idx)
      {
        links_[idx]->SetWorldTwist(math::Vector3(), math::Vector3());
      }
      model_->SetWorldTwist(math::Vector3(), math::Vector3());
    }

    bool check_foot(const std::string& foot, double &distance)
    {
      physics::LinkPtr foot_l = model_->GetLink(foot + "_leg_ankle_roll");
      math::Vector3 axis = foot_l->GetWorldPose().rot.GetYAxis();
      // this is the cosine of the angle between the world's Z axis and the
      // vector normal to the foot's plane
      double cos_vertical = axis.z / axis.GetLength();
      math::Box bb = foot_l->GetBoundingBox();
      math::Vector3 position = bb.GetCenter();
      distance = -position.y;
      bool good_contact = bb.min.z < 0.0025 and cos_vertical > 0.98;
      return good_contact;
    }

    void init_robot()
    {

      // Set joints to initial position and clean forces
      for (CIter it = osc_dict_.begin(); it != osc_dict_.end(); ++it)
      {
        it->first->SetForce(0, 0);
        it->first->SetAngle(0, it->second(0));
      }

      // take robot's left foot to (0,0), parallel to the ground
      math::Vector3 position(0,0,0);
      math::Quaternion rotation(0.70711, 0.70711, 0, 0);
      physics::LinkPtr left_foot = model_->GetLink("left_leg_ankle_roll");
      math::Pose rel_pose = left_foot->GetRelativePose();
      rotation = rotation * rel_pose.rot.GetInverse();
      position -= rotation * rel_pose.pos;
      math::Pose pose(position, rotation);
      model_->SetWorldPose(pose);
      // Now we put the model in the ground
      double delta_z = - getMinimumZ();
      pose.pos.z += delta_z;
      model_->SetWorldPose(pose);
      // nullify twist in each of the links
      nullify_twist();
      // give it a gentle push
      model_->SetWorldTwist(math::Vector3(0,-config_.begin_vel,0),
                            math::Vector3());
    }

    bool set_oscillators(const std::vector<std::string>& joint_names,
                         double T,
                         const std::vector<double>& A,
                         const std::vector<double>& phi,
                         const std::vector<double>& C,
                         bool mirror)
    {
      if (T <= 0)
      {
        ROS_ERROR("Non-positive period (%f)", T);
        return false;
      }

      if (joint_names.size() != A.size() or
          joint_names.size() != C.size() or
          joint_names.size() != phi.size())
      {
        ROS_ERROR("Vectors of different size");
        return false;
      }

      for (int i = 0; i < joint_names.size(); ++i)
      {
        Oscillator oscillator(T, A[i], C[i], phi[i]);
        bool set = set_oscillator(joint_names[i], oscillator);
        if (not set)
        {
          ROS_WARN("Could not set joint %s. Skipping...",
                   joint_names[i].c_str());
          continue;
        }

        int back = joint_names[i].length() - 1;
        if (mirror and joint_names[i][back] == 'l')
        {
          std::string mirror_j = joint_names[i].substr(0, back) + 'r';
          set_oscillator(mirror_j, oscillator.mirror());
        }
      }

      return true;
    }

  public:
    void reconfigure(darwin_gazebo::DarwinGazeboConfig& config, uint32_t level)
    {
      config_ = config;
    }

    virtual void Load(physics::ModelPtr parent, sdf::ElementPtr /*sdf*/)
    {
      if (not ros::isInitialized())
      {
        nh_ = NULL;
        ROS_ERROR("ROS has not been initialized. Cannot load plugin.");
        return;
      }

      nh_ = new ros::NodeHandle;

      model_ = parent;
      links_ = model_->GetLinks();

      server_ = new dynamic_reconfigure::Server<
        darwin_gazebo::DarwinGazeboConfig>(ros::NodeHandle("dfc"));
      dynamic_reconfigure::Server<
        darwin_gazebo::DarwinGazeboConfig>::CallbackType f;
      f = boost::bind(&DarwinExtra::reconfigure, this, _1, _2);
      server_->setCallback(f);
  
      prepare_controller();

      simulate_individual_srv_ = nh_->advertiseService(
          "darwin/simulate_individual",
          &DarwinExtra::SimulateIndividualCB, this);

      updateConnection_ = event::Events::ConnectWorldUpdateBegin(
          boost::bind(&DarwinExtra::OnUpdate, this, _1));

      ROS_INFO("DarwinExtra plugin loaded");
    }

    void OnUpdate(const common::UpdateInfo info)
    {
        double t = info.simTime.Double();

        set_joint_efforts(t);

        math::Quaternion rotation = model_->GetLink("base_link")->
          GetWorldPose().rot;

        math::Vector3 rpy(rotation.GetRoll(),
                          rotation.GetPitch(),
                          rotation.GetYaw());
        rpy_.push_back(rpy);

        math::Vector3 axis = rotation.GetYAxis();
        double cos_vertical = axis.z / axis.GetLength();

        // Do not bother checking if there is a goot contact between the feet
        // and the ground if the angle of the body with respect to the vertical
        // is too big
        bool contact = false;
        if (cos_vertical > 0.707)
        {
          double distance_l, distance_r;
          if (check_foot("left", distance_l))
          {
            if (distance_l > distance_)
            {
              distance_ = distance_l;
            }
            contact = true;
          }
          if (check_foot("right", distance_r))
          {
            if (distance_r > distance_)
            {
              distance_ = distance_r;
            }
            contact = true;
          }
        }
        
        no_contact_counter_ = contact? 0 : no_contact_counter_ + 1;
    }

    bool SimulateIndividualCB(darwin_gazebo::SimulateIndividual::Request&  req,
                              darwin_gazebo::SimulateIndividual::Response& res)
    {

      // Get and reset world
      physics::WorldPtr world = model_->GetWorld();

      // Pause (in case it wasn't already paused) and reset world
      world->SetPaused(true);
      world->Reset();

      // Set RNG seed for reproducible results
      math::Rand::SetSeed(73);
      world->GetPhysicsEngine()->SetSeed(42);

      // Initialize robot position and oscillators
      set_oscillators(req.joint_names, req.T, req.A, req.phi, req.C, true);
      init_robot();

      // Clear monitoring measures of last individual
      rpy_.clear();
      distance_ = 0;
      no_contact_counter_ = 0;

      common::Time end(req.max_duration);

      // Step by step simulation to maximise reproducibility
      while (world->GetSimTime() <= end and no_contact_counter_ < 125)
      {
        world->StepWorld(1);
      }

      res.theta = compute_theta();
      res.distance = distance_;
      res.duration = world->GetSimTime().Double();

      return true;
    }

    ~DarwinExtra()
    {
      if (nh_ != NULL) delete nh_;
      if (server_ != NULL) delete server_;
    }

};

GZ_REGISTER_MODEL_PLUGIN(DarwinExtra)
}
